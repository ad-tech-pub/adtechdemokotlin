package com.adtech.adtechdemokotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.constraintlayout.widget.ConstraintLayout
import com.adtech.adtechmobile.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var adtInterAd: ADTInterstitial

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Initialize
        ADTechMobile.initialize(this)
        ADTRequestConfiguration.testMode = true
        adtInterAd = ADTInterstitial(this)

        testBannerXML()
        testBannerProgrammatic()
        testInterstitial()
    }

    fun testBannerXML() {
        val adView = bannerAd;
        adView.adSize = ADTAdSize.HEIGHT_90
        adView.adListener = object : ADTAdListener {
            override fun onAdLoaded() {
                Log.d("Test App", "ADT BannerXML loaded")
            }

            override fun onAdFailedToLoad(errorCode: ADTAdError) {
                Log.d("Test App", "ADT BannerXML failed: code " + errorCode)
            }
        }
        adView.request()
    }

    fun testBannerProgrammatic() {
        // Create banner ad
        val adView = ADTBannerView(this)
        // Tell parent layout how to render the adview
        val layout = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        adView.layoutParams = layout
        // Add to parent layout
        activityLayout.addView(adView)

        adView.adSize = ADTAdSize.HEIGHT_50
        adView.adListener = object : ADTAdListener {
            override fun onAdLoaded() {
                Log.d("Test App", "ADT Banner loaded")
            }

            override fun onAdFailedToLoad(errorCode: ADTAdError) {
                Log.d("Test App", "ADT Banner failed: code " + errorCode)
            }
        }
        adView.request()

    }

    fun testInterstitial() {
        adtInterAd.adListener = object : ADTAdListener {
            override fun onAdLoaded() {
                Log.d("Test App", "ADT Interstitial loaded")
            }

            override fun onAdFailedToLoad(errorCode: ADTAdError) {
                Log.d("Test App", "ADT Interstitial failed: code " + errorCode)
            }
            override fun onAdClosed() {
                adtInterAd.request();
            }
        }
        adtInterAd.request()
        myButton.setOnClickListener {
            if (adtInterAd.isReady) {
                adtInterAd.show()
            }
        }
    }
}